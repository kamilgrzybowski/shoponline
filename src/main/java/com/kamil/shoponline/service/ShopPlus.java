package com.kamil.shoponline.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@Profile("plus")
public class ShopPlus extends ShopStart {
    @Value("${vat}")
    protected double vat;
    protected double priceWithVat;

    @EventListener(ApplicationReadyEvent.class)
    @Override
    public void getProduct(){
        priceWithVat = productsCosts * vat;
        System.out.println("Total price with VAT: " + priceWithVat);
    }

    public double getVat() {
        return vat;
    }
}
