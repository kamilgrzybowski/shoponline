package com.kamil.shoponline.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@Profile("pro")
public class ShopPro extends ShopPlus {
    @Value("${discount}")
    protected double discount;
    protected double priceWithVatAndDiscount;

    public double getDiscount() {
        return discount;
    }

    @EventListener(ApplicationReadyEvent.class)
    @Override
    public void getProduct(){
        priceWithVatAndDiscount = priceWithVat * discount;
        System.out.println("This is price with VAT and DISCOUNT: " + priceWithVatAndDiscount);
    }

}
