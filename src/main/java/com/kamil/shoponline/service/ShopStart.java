package com.kamil.shoponline.service;

import com.kamil.shoponline.repository.Product;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ShopStart {
    private List<Product>products = new ArrayList<>();
    protected double productsCosts = 0;

    public ShopStart() {
        products.add(new Product("Basketball", randomPrice()));
        products.add(new Product("Shoes", randomPrice()));
        products.add(new Product("Jersey", randomPrice()));
        products.add(new Product("Shorts", randomPrice()));
        products.add(new Product("Handband", randomPrice()));
    }

    private int randomPrice() {
        int minPrice = 50;
        int maxPrice = 250;
        return new Random().nextInt((maxPrice) + 1) + minPrice;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void helloCustomer(){
        System.out.println();
        System.out.println("Hello Customer");
    }

    @EventListener(ApplicationReadyEvent.class)
    public void getProduct(){

        System.out.println("YOUR BASKET: ");
        products.forEach(System.out::println);
        for (Product item:products) {
            productsCosts+=item.getPrice();
        }
        System.out.println("Total price = " + productsCosts + "zl");
    }




}
