package com.kamil.shoponline.repository;

import org.springframework.stereotype.Repository;

@Repository
public class Product {
    private String name;
    private double price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Product() {
    }

    public String getName() {
        return name;
    }


    public double getPrice() {
        return price;
    }


    @Override
    public String toString() {
        return "    Product{" +
                "name='" + name + '\'' +
                ", price=" + price + "zl" +
                '}';
    }
}
